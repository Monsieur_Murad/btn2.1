/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:48
 */
#include <iostream>
#include <string>
using std::string;
using namespace std;
#include <set>
using std::set;
#include <iomanip>
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}


	/* TODO: write me... */
	int words=0;
	string ch;
	int linescount=0;
	ch += '\n'; // put newline back


  int lines=0;
	while(getline(cin,ch)){
		//for (char c: str ) if (c == '\n' )
			++lines;

		//{
		//if (!str.empty() && str.back() != '\n') ++lines;
	bool readnws = false;
	for(size_t i = 0; i<ch.length(); i++){

		linescount += 1;

		if (readnws == false) {
			if (!isspace(ch[i])) {
				readnws = true;

				// start new word with ch[i] as only character
			} else {

				// reading more white space; just increase char count
			}
		}
		else {
			if (!isspace(ch[i])) {
				// add ch[i] to current word
			} else {
				readnws = false;
				// increment word count
				words++;
				// also, add current word to set of unique words
			}
		}//else

	}
}
//words+=lines;
	cout<< "\n"<<'\t'<< lines <<'\t'<< (words)<<'\t'<< linescount<< '\t'  << endl;

	return 0;
}

